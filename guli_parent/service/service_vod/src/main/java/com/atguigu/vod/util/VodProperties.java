package com.atguigu.vod.util;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: panDengZhu
 * @date: 2024/3/27 0:27
 * @version: 1.0
 */

@Data
@Component
@ConfigurationProperties(prefix = "aliyun.vod.file")
public class VodProperties implements InitializingBean {

    private String keyId;

    private String keySecret;

    public static String ACCESS_KEY_ID;
    public static String ACCESS_KEY_SECRET;


    @Override
    public void afterPropertiesSet() throws Exception {
        ACCESS_KEY_ID = keyId;
        ACCESS_KEY_SECRET = keySecret;
    }
}
