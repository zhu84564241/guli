package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.eduservice.subject.OneSubject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-18
 */
//@CrossOrigin
@Slf4j
@Api(tags = "课程分类管理")
@RestController
@RequestMapping("/eduservice/subject")
public class EduSubjectController {

    @Resource
    private EduSubjectService eduSubjectService;

    /**
     * 添加课程分类
     * 获取上传过来文件，把文件内容读取出来
     *
     * @param file excel文件
     * @return
     */
    @PostMapping("/addSubject")
    @ApiOperation("上传Excel文件")
    public R addSubject(@ApiParam(value = "Excel文件", required = true)
                        @RequestBody MultipartFile file) {
        //上传过来excel文件
        eduSubjectService.addSubject(file, eduSubjectService);
        return R.ok();
    }

    /**
     * 课程分页列表(树形)
     * @return
     */
    @ApiOperation("课程分页列表(树形)")
    @GetMapping("/getAllSubject")
    public R getAllSubject() {
        //list集合泛型是一级分类
        List<OneSubject> list =eduSubjectService.getAllSubject();
        return R.ok().data("list",list);
    }

}

