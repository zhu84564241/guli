package com.atguigu.eduservice.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @descriptions: 课程发布信息封装类
 * @author: pandengZhu
 * @date: 2024/3/25 11:19
 * @version: 1.0
 */

@ApiModel(value = "课程发布信息")
@Data
public class CoursePublishVo implements Serializable {

    private static  final long serialVersionUID = 1l;

    private String title;
    private String cover;
    private Integer lessonNum;
    private String subjectLevelOne;
    private String subjectLevelTwo;
    private String teacherName;
    private String price;//只用于显示
}
