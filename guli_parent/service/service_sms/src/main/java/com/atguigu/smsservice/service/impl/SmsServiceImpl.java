package com.atguigu.smsservice.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponse;
import com.atguigu.commonutils.ResponseEnum;
import com.atguigu.servicebase.exception.BusinessException;
import com.atguigu.smsservice.service.SmsService;
import com.atguigu.smsservice.utils.SmsProperties;
import com.google.gson.Gson;
import darabonba.core.client.ClientOverrideConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @descriptions:
 * @author: pandengZhu
 * @date: 2024/4/9 14:35
 * @version: 1.0
 */

@Service
@Slf4j
public class SmsServiceImpl implements SmsService {

    @Override
    public void sendSmsCode(String phone, String templateCode, Map<String, Object> param, String smsCode) {


        StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
                .accessKeyId(SmsProperties.KEY_ID)
                .accessKeySecret(SmsProperties.KEY_SECRET)
                .build());

        AsyncClient client = AsyncClient.builder()
                .region(SmsProperties.REGION_ID) // Region ID
                .credentialsProvider(provider)

                .overrideConfiguration(
                        ClientOverrideConfiguration.create()
                                .setEndpointOverride("dysmsapi.aliyuncs.com")
                )
                .build();

        SendSmsRequest sendSmsRequest = SendSmsRequest.builder()
                .signName(SmsProperties.SIGN_NAME)
                .templateCode(SmsProperties.TEMPLATE_CODE)
                .phoneNumbers(phone)
                .templateParam("{\"code\":\"" + smsCode + "\"}")
                .build();

        CompletableFuture<SendSmsResponse> response = client.sendSms(sendSmsRequest);

        SendSmsResponse resp = null;
        try {
            resp = response.get();
            //获取响应结果
            String data = new Gson().toJson(resp);
            JSONObject jsonObject = JSONObject.parseObject(data);
            String code = (String) jsonObject.getJSONObject("body").get("code");
            String message = (String) jsonObject.getJSONObject("body").get("message");
            log.info("code==========" + code + "message==========" + message);

            //业务失败处理
            /*Assert.notEquals("isv.BUSINESS_LIMIT_CONTROL",code, ResponseEnum.ALIYUN_SMS_LIMIT_CONTROL_ERROR);
            Assert.equals("OK",code,ResponseEnum.ALIYUN_SMS_ERROR);*/

        } catch (InterruptedException e) {
            throw new BusinessException(ResponseEnum.ALIYUN_SMS_ERROR,e);
        } catch (ExecutionException e) {
            throw new BusinessException(ResponseEnum.ALIYUN_SMS_ERROR,e);
        }
        System.out.println(new Gson().toJson(resp));

        client.close();

    }

    /**
     * 判断手机号是否注册
     * @param phone
     * @return
     */
  /*  @Override
    public boolean checkMobile(String phone) {
        QueryWrapper<>

        return false;
    }*/
}


