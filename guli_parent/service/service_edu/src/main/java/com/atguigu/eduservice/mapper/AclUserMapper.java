package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.AclUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author panDengZhu
 * @since 2024-04-22
 */
public interface AclUserMapper extends BaseMapper<AclUser> {

}
