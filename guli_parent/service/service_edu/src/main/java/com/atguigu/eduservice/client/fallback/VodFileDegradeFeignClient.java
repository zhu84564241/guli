package com.atguigu.eduservice.client.fallback;

import com.atguigu.eduservice.client.VodClient;
import com.atguigu.commonutils.R;
import com.atguigu.commonutils.ResponseEnum;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @descriptions: 熔断器的实现类
 * @author: pandengZhu
 * @date: 2024/3/30 20:18
 * @version: 1.0
 */
@Component
public class VodFileDegradeFeignClient implements VodClient {


    @Override
    public R removeVideo(String videoId) {
        return R.error().code(ResponseEnum.DELETE_VIDEO_ERROR.getCode()).message(ResponseEnum.DELETE_VIDEO_ERROR.getMessage());
    }

    @Override
    public R deleteBatch(List<String> videoIdList) {
        return R.error().code(ResponseEnum.DELETE_BATCH_VIDEO_ERROR.getCode()).message(ResponseEnum.DELETE_BATCH_VIDEO_ERROR.getMessage());
    }
}
