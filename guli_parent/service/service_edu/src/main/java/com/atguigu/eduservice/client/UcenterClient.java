package com.atguigu.eduservice.client;

import com.atguigu.eduservice.client.fallback.UcenterClientFallback;
import com.atguigu.eduservice.entity.EduComment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @descriptions: 获取会员信息微服务调用
 * @author: pandengZhu
 * @date: 2024/4/22 12:49
 * @version: 1.0
 */

@FeignClient(value = "service-ucenter", fallback = UcenterClientFallback.class)
@Component
public interface UcenterClient {

    @GetMapping("/educenter/member/getMemberInfo/{memberId}")
    public EduComment getMemberInfo(@PathVariable("memberId") String memberId);
}
