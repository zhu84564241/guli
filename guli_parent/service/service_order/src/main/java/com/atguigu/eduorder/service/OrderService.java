package com.atguigu.eduorder.service;

import com.atguigu.eduorder.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-03-13
 */
public interface OrderService extends IService<Order> {



    /**
     * 根据课程id和 用户id创建订单，返回订单id
     *
     * @param courseId  课程id
     * @param memberId   会员id
     * @return  订单号
     */
    String createOrder(String courseId, String memberId);
}
