package com.atguigu.vod.controller;


import com.atguigu.commonutils.R;
import com.atguigu.vod.service.VodService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @descriptions: 视频点播控制层
 * @author: pandengZhu
 * @date: 2024/3/27 0:22
 * @version: 1.0
 */
@Api(tags = "视频点播")
//@CrossOrigin
@RestController
@RequestMapping("/eduVod/video")
public class VodController {

    @Resource
    private VodService vodService;

    /**
     * 上传视频
     *
     * @param file 视频文件
     * @return 视频id
     */

    @ApiOperation("上传视频")
    @PostMapping("/uploadAlyVideo")
    public R uploadAlyVideo(@ApiParam(value = "视频文件", required = true)
                            @RequestBody MultipartFile file) {

        String videoId = vodService.uploadAlyVideo(file);
        return R.ok().data("videoId", videoId);
    }

    @ApiOperation("删除视频")
    @DeleteMapping("/deleteVideo/{id}")
    public R removeVideo(@ApiParam(value = "云端视频资源id", required = true)
                         @PathVariable String id) {

        vodService.removeVideo(id);
        return R.ok().data("videoId", id);
    }

    @ApiOperation("批量删除视频")
    @DeleteMapping("/delete-batch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList) {
        try {
            vodService.removeMoreAlyVideo(videoIdList);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return R.ok();
    }

    /**
     * 获取视频播放凭证
     *
     * @param id
     * @return
     */
    @ApiOperation("获取视频凭证")
    @GetMapping("/getVideoPlayAuth/{id}")
    public R getVideoPlayAuth(@ApiParam(value = "视频id", required = true)
                              @PathVariable String id) {

        String playAuth = vodService.getVideoPlayAuth(id);
        //System.out.println("playAuth==================================" + playAuth);
        return R.ok().data("playAuth", playAuth);

    }

}

