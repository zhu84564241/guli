package com.atguigu.eduservice.subject;

import lombok.Data;

/**
 * @descriptions: 二级分类
 * @author: pandengzhu
 * @date: 2024/3/19 10:52
 * @version: 1.0
 */

@Data
public class TwoSubject {

    private String id;
    private String title;
}
