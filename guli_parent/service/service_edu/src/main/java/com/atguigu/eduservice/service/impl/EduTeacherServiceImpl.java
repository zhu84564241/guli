package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.TeacherQuery;
import com.atguigu.eduservice.mapper.EduTeacherMapper;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2024-03-02
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {


    /**
     * 分页查询讲师列表
     *
     * @param pageParam
     * @param teacherQuery
     * @return
     */
    @Override
    public IPage<EduTeacher> listPage(Page<EduTeacher> pageParam, TeacherQuery teacherQuery) {
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();

        QueryWrapper<EduTeacher> queryWrapper = new QueryWrapper<>();
        //queryWrapper.orderByAsc("sort");
        if (teacherQuery == null) {
            return baseMapper.selectPage(pageParam, queryWrapper);

        }
        queryWrapper.like(StringUtils.isNotBlank(name), "name", name)
                .eq(level != null, "level", level)
                .ge(StringUtils.isNotBlank(begin), "gmt_create", begin)
                .le(StringUtils.isNotBlank(end), "gmt_create", end)
                .orderByDesc("gmt_create");

        return baseMapper.selectPage(pageParam, queryWrapper);
    }

    /**
     * 分页查询讲师列表
     *
     * @param pageParam
     * @return
     */
    @Override
    public Map<String, Object> getTeacherFrontList(Page<EduTeacher> pageParam) {
        QueryWrapper<EduTeacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        //把分页数据封装到pageParam对象
        baseMapper.selectPage(pageParam, queryWrapper);

        List<EduTeacher>records = pageParam.getRecords();
        long current = pageParam.getCurrent();//当前页
        long pages = pageParam.getPages();//总页数
        long total = pageParam.getTotal();//当前页记录数
        long size = pageParam.getSize();//总记录数
        boolean hasNext = pageParam.hasNext();//下一页
        boolean hasPrevious = pageParam.hasPrevious();//上一页

        //把分页数据获取出来，放到map集合
        Map<String,Object> map =new HashMap<>();
        map.put("items",records);
        map.put("current",current);
        map.put("pages",pages);
        map.put("total",total);
        map.put("size",size);
        map.put("hasNext",hasNext);
        map.put("hasPrevious",hasPrevious);

        return map;
    }


}
