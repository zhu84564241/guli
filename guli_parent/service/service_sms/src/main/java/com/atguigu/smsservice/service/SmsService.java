package com.atguigu.smsservice.service;

import java.util.Map;

/**
 * @descriptions:
 * @author: pandengZhu
 * @date: 2024/4/9 14:34
 * @version: 1.0
 */
public interface SmsService {

    /**
     * 发送短信
     * @param phone
     * @param templateCode
     * @param param
     * @param smsCode
     */
    void sendSmsCode(String phone, String templateCode, Map<String, Object> param, String smsCode);

    /**
     * 判断手机号是否注册
     * @param phone
     * @return
     */
    //boolean checkMobile(String phone);
}
