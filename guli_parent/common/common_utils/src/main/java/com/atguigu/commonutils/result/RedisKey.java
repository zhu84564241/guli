package com.atguigu.commonutils.result;

/**
 * @description:
 * @author: pandeng_zhu
 * @time: 2023/2/15 13:07
 */

public class RedisKey {

    //数据字典列表数据
    public static final String GULI_BANNER_LIST = "GULI:BANNER:LIST:";

    //阿里云短信验证码
    public static final String GULI_SMS_CODE = "GULI:SMS:CODE";
}
