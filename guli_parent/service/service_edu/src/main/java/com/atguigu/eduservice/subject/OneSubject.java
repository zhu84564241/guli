package com.atguigu.eduservice.subject;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @descriptions: 一级分类
 * @author: pandengzhu
 * @date: 2024/3/19 10:51
 * @version: 1.0
 */

@Data
public class OneSubject {

    private String id;
    private String title;

    private List<TwoSubject> children = new ArrayList<>();

}
