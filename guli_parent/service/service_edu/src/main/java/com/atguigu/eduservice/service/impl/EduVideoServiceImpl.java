package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.client.VodClient;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.mapper.EduVideoMapper;
import com.atguigu.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Resource
    private VodClient vodConfig;

    /**
     * 删除小结
     *
     * @param courseId
     */
    @Override
    public boolean removeVideoByCourseId(String courseId) {

        //1.根据课程id查询课程所有的视频id
        QueryWrapper<EduVideo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", courseId);
        queryWrapper.select("video_source_id");
        List<EduVideo> eduVideoList = baseMapper.selectList(queryWrapper);
        //List<EduVideo>变成List<String>
        List<String> eduSourceIdList = new ArrayList<>();

        for (EduVideo eduVideo : eduVideoList) {
            //2.遍历所有的eduVideo，获取所有的videoSourceId
            String videoSourceId = eduVideo.getVideoSourceId();
            //判断videoSourceId是否为空
            if (StringUtils.isNotBlank(videoSourceId)) {
                eduSourceIdList.add(videoSourceId);
            }
        }
        //调用vod服务批量删除远程视频
        if (eduSourceIdList.size() > 0) {
            vodConfig.deleteBatch(eduSourceIdList);
        }

        QueryWrapper<EduVideo> eduVideoQueryWrapper = new QueryWrapper<>();
        eduVideoQueryWrapper.eq("course_id", courseId);
        Integer count = baseMapper.delete(eduVideoQueryWrapper);


        //如果count>0并且不等于null返回null
        return null != count && count > 0;
    }
}
