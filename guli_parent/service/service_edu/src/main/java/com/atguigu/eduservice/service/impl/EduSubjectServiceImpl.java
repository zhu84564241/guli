package com.atguigu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.ExcelSubject;
import com.atguigu.eduservice.listener.ExcelSubjectListener;
import com.atguigu.eduservice.mapper.EduSubjectMapper;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.eduservice.subject.OneSubject;
import com.atguigu.eduservice.subject.TwoSubject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-18
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    /**
     * 添加课程分类
     * 获取上传过来文件，把文件内容读取出来
     *
     * @param file excel文件
     *
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void addSubject(MultipartFile file, EduSubjectService eduSubjectService) {

        try {
            //1.获取文件输入流
            InputStream inputStream = file.getInputStream();
            //这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
            EasyExcel.read(inputStream, ExcelSubject.class, new ExcelSubjectListener(eduSubjectService)).sheet().doRead();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 课程分页列表(树形)
     * @return
     */
    @Override
    public List<OneSubject> getAllSubject() {
        //1 查询所有一级分类  parentid = 0
        QueryWrapper<EduSubject>wrapperOne =new QueryWrapper<>();
        wrapperOne.eq("parent_id","0");
        List<EduSubject> oneSubjectList = baseMapper.selectList(wrapperOne);

        //2 查询所有二级分类  parentid != 0
        QueryWrapper<EduSubject>wrapperTwo = new QueryWrapper<>();
        wrapperTwo.ne("parent_id","0");
        List<EduSubject> twoSubjectList = baseMapper.selectList(wrapperTwo);

        //创建list集合，用于存储最终封装数据
        List<OneSubject> oneFinalSubjectList= new ArrayList<>();

        //3 封装一级分类
        //查询出来所有的一级分类list集合遍历，得到每个一级分类对象，获取每个一级分类对象值，
        //封装到要求的list集合里面 List<OneSubject> finalSubjectList
        for (EduSubject eduSubject : oneSubjectList) {
            //得到oneSubjectList每个eduSubject对象
            OneSubject oneSubject =new OneSubject();
            BeanUtils.copyProperties(eduSubject,oneSubject);
            oneFinalSubjectList.add(oneSubject);

            //在一级分类循环遍历查询所有的二级分类
            //创建list集合封装每个一级分类的二级分类
            List<TwoSubject> twoFinalSubjectList = new ArrayList<>();

            for (EduSubject subject : twoSubjectList) {
                //判断二级分类parentid和一级分类id是否一样
                if (subject.getParentId().equals(eduSubject.getId())) {
                    TwoSubject twoSubject = new TwoSubject();
                    //得到TwoSubjectList每个eduSubject对象
                    BeanUtils.copyProperties(subject, twoSubject);
                    twoFinalSubjectList.add(twoSubject);
                }
            }
            //把一级下面所有二级分类放到一级分类里面
            oneSubject.setChildren(twoFinalSubjectList);
        }

        return oneFinalSubjectList;
    }
}
