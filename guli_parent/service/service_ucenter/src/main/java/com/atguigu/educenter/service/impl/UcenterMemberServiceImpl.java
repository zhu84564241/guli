package com.atguigu.educenter.service.impl;

import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.MD5;
import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.atguigu.educenter.service.UcenterMemberService;
import com.atguigu.educenter.mapper.UcenterMemberMapper;
import com.atguigu.commonutils.ResponseEnum;
import com.atguigu.servicebase.exception.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-04-10
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    /**
     * 用户注册
     *
     * @param registerVo
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void register(RegisterVo registerVo) {

        //1.查询数据库判断手机号是否被注册
        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        String mobile = registerVo.getMobile();
        queryWrapper.eq("mobile", mobile);
        Integer count = baseMapper.selectCount(queryWrapper);
        //    MOBILE_EXIST_ERROR(-113,"手机号已被注册"),
        Assert.isTrue(count == 0, ResponseEnum.MOBILE_EXIST_ERROR);

        //2.插入用户基本信息
        UcenterMember member = new UcenterMember();
        member.setMobile(registerVo.getMobile());
        member.setPassword(MD5.encrypt(registerVo.getPassword()));
        member.setNickname(registerVo.getNickName());
        member.setIsDisabled(false);
        //设置一张静态资源服务器上的一张照片
        member.setAvatar("https://guli-files00.oss-cn-beijing.aliyuncs.com/%E5%A4%B4%E5%83%8F/r53tj5abbfv.jpg");
        baseMapper.insert(member);

    }

    /**
     * 用户登录
     *
     * @param ucenterMember
     */

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String login(UcenterMember ucenterMember) {

        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();

        //1.获取用户
        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile", mobile);
        UcenterMember member = baseMapper.selectOne(queryWrapper);
        //2.判断用户是否存在
        Assert.notNull(member, ResponseEnum.LOGIN_MOBILE_ERROR);

        //3.判断密码是否正确
        Assert.equals(MD5.encrypt(password), member.getPassword(), ResponseEnum.LOGIN_PASSWORD_ERROR);
        //4.判断账号是否被禁用
        Assert.equals(member.getIsDisabled(), UcenterMember.STATUS_NORMAL, ResponseEnum.ACCOUNT_DISABLE);

        //5.生成token
        //String token = JwtUtil.createToken(member.getId(), member.getNickname());
        String token = JwtUtils.getJwtToken(member.getId(), member.getNickname());
        System.out.println("token============================             " + token);
        return token;

    }

    /**
     * 根据token获取登录信息
     *
     * @param userId
     * @return
     */
    @Override
    public UcenterMember getLoginById(String userId) {
        UcenterMember member = baseMapper.selectById(userId);
        return member;
    }

    /**
     * 根据openid查询会员信息
     *
     * @param openid
     * @return
     */
    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid", openid);
        UcenterMember member = baseMapper.selectOne(queryWrapper);

        return member;
    }

    /**
     * 统计某一天的注册人数
     *
     * @param day
     * @return
     */
    @Override
    public Integer countRegister(String day) {

        return baseMapper.countRegister(day);
    }
}
