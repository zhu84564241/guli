package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.TeacherQuery;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-02
 */
@Api(tags = "讲师管理")
//@CrossOrigin
@RestController
@RequestMapping("/eduservice/teacher")
public class EduTeacherController {

    @Resource
    private EduTeacherService eduTeacherService;


    /**
     * 1.查询讲师所有数据
     */
    @ApiOperation("所有讲师列表")
    @GetMapping("/findAll")
    public R list() {
        List<EduTeacher> list = eduTeacherService.list(null);
        return R.ok().data("list", list).message("查询成功");

    }

    /**
     * 2.讲师逻辑删除功能
     */
    @ApiOperation("根据id删除讲师")
    @DeleteMapping("/{id}")
    public R removeById(@ApiParam(value = "讲师ID", required = true)
                        @PathVariable String id) {
        boolean result = eduTeacherService.removeById(id);
        if (result) {
            return R.ok().message("删除成功");
        } else {
            return R.ok().message("删除失败");
        }
    }

    /**
     * 分页带条件查询教师列表
     * @param page      当前页码
     * @param limit     每页记录数
     * @param teacherQuery
     * @return
     */
    @ApiOperation("分页带条件查询教师列表")
    @PostMapping("/pageTeacherCondition/{page}/{limit}")
    public R pageTeacherCondition(@ApiParam(value = "当前页码", required = true)
                                      @PathVariable Long page,
                                  @ApiParam(value = "每页记录数", required = true)
                                  @PathVariable Long limit,
                                  @ApiParam(value = "查询条件对象", required = false)
                                      @RequestBody TeacherQuery teacherQuery) {
        Page<EduTeacher> pageParam = new Page<>(page, limit);
        IPage<EduTeacher> pageModel = eduTeacherService.listPage(pageParam, teacherQuery);
        return R.ok().data("pageModel", pageModel).message("查询成功");
    }

    /**
     * 4.添加讲师
     *
     * @param eduTeacher
     * @return
     */
    @ApiOperation("添加讲师")
    @PostMapping("/addTeacher")
    public R addTeacher(@ApiParam(value = "讲师对象", required = true)
                  @RequestBody EduTeacher eduTeacher) {
        boolean result = eduTeacherService.save(eduTeacher);
        if (result) {
            return R.ok().message("添加成功");
        } else {
            return R.error().message("添加失败");
        }
    }

    /**
     * 5.根据id查询讲师
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询讲师")
    @GetMapping("getTeacher/{id}")
    public R getTeacherById(@ApiParam(value = "讲师ID", required = true)
                     @PathVariable String id) {
        EduTeacher eduTeacher = eduTeacherService.getById(id);
        return R.ok().data("eduTeacher", eduTeacher);

    }

    @ApiOperation(value = "修改讲师")
    @PostMapping("/updateTeacher")
    public R updateTeacher(@ApiParam(value = "讲师对象", required = true)
                        @RequestBody EduTeacher eduTeacher) {
        boolean result = eduTeacherService.updateById(eduTeacher);

        if (result) {
            return R.ok().message("修改成功");
        } else {
            return R.error().message("修改失败");
        }
    }

}

