package com.atguigu.educenter.mapper;

import com.atguigu.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author pandengzhu
 * @since 2024-04-10
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {


    /**
     * 查询某一天注册人数
     * @param day
     * @return
     */
    Integer countRegister(String day);
}
