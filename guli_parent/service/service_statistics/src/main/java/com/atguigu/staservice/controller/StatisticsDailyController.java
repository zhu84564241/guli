package com.atguigu.staservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.staservice.service.StatisticsDailyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author panDengZhu
 * @since 2024-05-17
 */

@Api(tags = "统计分析")
@RestController
//@CrossOrigin
@RequestMapping("/staservice/sta")
public class StatisticsDailyController {

    @Resource
    private StatisticsDailyService staService;

    @ApiOperation("统计某一天注册人数,生成统计数据")
    @GetMapping("/registerCount/{day}")
    public R registerCount(@ApiParam(value = "日期", required = true)
                           @PathVariable String day) {
        staService.registerCount(day);
        return R.ok();
    }

    /**
     * 图表显示，返回两部分数据，日期json数组，数量json数组
     * @param type  统计日期数量
     * @param begin 开始查询时间
     * @param end   结束查询时间
     * @return
     */
    @ApiOperation("图标显示")
    @GetMapping("showData/{type}/{begin}/{end}")
    public R showData(@ApiParam(value = "统计日期数量", required = true) @PathVariable String type,
                      @ApiParam(value = "开始查询时间", required = true) @PathVariable String begin,
                      @ApiParam(value = "结束查询时间", required = true) @PathVariable String end) {

        Map<String, Object> map = staService.getShowData(type, begin, end);
        return R.ok().data(map);
    }

}

