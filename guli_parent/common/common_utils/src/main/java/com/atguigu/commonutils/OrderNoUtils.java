package com.atguigu.commonutils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.SimpleFormatter;

/**
 * @descriptions: 订单号工具类
 * @author: pandengZhu
 * @date: 2024/4/24 21:01
 * @version: 1.0
 */
public class OrderNoUtils {

    public static String getOrderNo() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmsss");
        String newDate = sdf.format(new Date());
        String result = "";
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            result += random.nextInt(10);
        }
        return newDate + result;

    }
}
