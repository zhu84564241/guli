package com.atguigu.servicebase.handler;

import com.atguigu.commonutils.R;
import com.atguigu.servicebase.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author pandengzhu
 * @time 2024-03-12 14:25
 */

/**
 * 时间自动填充
 */
@Slf4j
@Component//Spring容易自动管理
@ControllerAdvice//在controller层添加通知。如果使用@ControllerAdvice，则方法上需要添加@ResponseBody
public class GlobalExceptionHandler {


    @ExceptionHandler(value = Exception.class)//当controller抛出exception，则捕获
    @ResponseBody
    public R handleException(Exception e) {
        log.error(e.getMessage());
        return R.error().message("执行全局异常处理。。。。。。。。");
    }

    /**
     * 自定义异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody //为了返回数据
    public R BusinessException(BusinessException e) {
        log.error(e.getMsg(), e);
        return R.error().code(e.getCode()).message(e.getMsg());
    }

    /**
     * 算数特定异常
     */
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody //为了返回数据
    public R error(ArithmeticException e) {
        e.printStackTrace();
        return R.error().message("执行了ArithmeticException异常处理..");
    }


}
