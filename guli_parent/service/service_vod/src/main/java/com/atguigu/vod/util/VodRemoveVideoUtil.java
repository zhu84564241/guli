package com.atguigu.vod.util;

import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.vod20170321.AsyncClient;
import com.aliyun.sdk.service.vod20170321.models.DeleteVideoRequest;
import com.aliyun.sdk.service.vod20170321.models.DeleteVideoResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.google.gson.Gson;
import darabonba.core.client.ClientOverrideConfiguration;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @descriptions: 阿里云删除视频工具类
 * @author: pandengZhu
 * @date: 2024/3/30 1:30
 * @version: 1.0
 */


public class VodRemoveVideoUtil {


    public static void removeVideo(String id) throws ExecutionException, InterruptedException {
        StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
                .accessKeyId(VodProperties.ACCESS_KEY_ID)
                .accessKeySecret(VodProperties.ACCESS_KEY_SECRET)
                .build());

        // Configure the Client
        AsyncClient client = AsyncClient.builder()
                .region("cn-shanghai") // Region ID
                .credentialsProvider(provider)
                .overrideConfiguration(
                        ClientOverrideConfiguration.create()
                                .setEndpointOverride("vod.cn-shanghai.aliyuncs.com")
                )
                .build();

        DeleteVideoRequest deleteVideoRequest = DeleteVideoRequest.builder()
                .videoIds(id)
                .build();

        CompletableFuture<DeleteVideoResponse> response = client.deleteVideo(deleteVideoRequest);
        DeleteVideoResponse resp = response.get();

        System.out.println(new Gson().toJson(resp));
        client.close();
    }


    public static void removeVideoList(List videoIdList)  {
        StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
                .accessKeyId(VodProperties.ACCESS_KEY_ID)
                .accessKeySecret(VodProperties.ACCESS_KEY_SECRET)
                .build());

        // Configure the Client
        AsyncClient client = AsyncClient.builder()
                .region("cn-shanghai") // Region ID
                .credentialsProvider(provider)
                .overrideConfiguration(
                        ClientOverrideConfiguration.create()
                                .setEndpointOverride("vod.cn-shanghai.aliyuncs.com")
                )
                .build();

        String videoId = StringUtils.join(videoIdList.toArray(), ",");

        DeleteVideoRequest deleteVideoRequest = DeleteVideoRequest.builder()
                .videoIds(videoId)
                .build();

        CompletableFuture<DeleteVideoResponse> response = client.deleteVideo(deleteVideoRequest);
        DeleteVideoResponse resp = null;
        try {
            resp = response.get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        System.out.println(new Gson().toJson(resp));
        client.close();
    }
}
