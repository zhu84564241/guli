package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
public interface EduVideoService extends IService<EduVideo> {

    /**
     * 删除小结
     * @param courseId
     */
    boolean removeVideoByCourseId(String courseId);
}
