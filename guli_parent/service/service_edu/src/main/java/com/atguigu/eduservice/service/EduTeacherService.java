package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.TeacherQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author atguigu
 * @since 2024-03-02
 */
public interface EduTeacherService extends IService<EduTeacher> {

    /**
     * 分页条件查询讲师列表
     *
     * @param pageParam
     * @param teacherQuery
     * @return
     */
    IPage<EduTeacher> listPage(Page<EduTeacher> pageParam, TeacherQuery teacherQuery);

    /**
     * 分页查询讲师列表
     * @param pageParam
     * @return
     */
    Map<String, Object> getTeacherFrontList(Page<EduTeacher> pageParam);

}
