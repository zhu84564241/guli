package com.atguigu.staservice.service;

import com.atguigu.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务类
 * </p>
 *
 * @author panDengZhu
 * @since 2024-05-17
 */
public interface StatisticsDailyService extends IService<StatisticsDaily> {

    /**
     * 统计某一天注册人数,生成统计数据
     * @param day
     */
    void registerCount(String day);

    /**
     * 图表显示，返回两部分数据，日期json数组，数量json数组
     * @param type  统计日期数量
     * @param begin 开始查询时间
     * @param end   结束查询时间
     * @return
     */
    Map<String, Object> getShowData(String type, String begin, String end);
}
