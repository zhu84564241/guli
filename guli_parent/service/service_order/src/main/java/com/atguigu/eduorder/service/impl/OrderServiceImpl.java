package com.atguigu.eduorder.service.impl;

import com.atguigu.commonutils.CourseWebVoOrder;
import com.atguigu.commonutils.OrderNoUtils;
import com.atguigu.commonutils.UcenterMemberOrder;
import com.atguigu.eduorder.client.EduClient;
import com.atguigu.eduorder.client.UcenterClient;
import com.atguigu.eduorder.entity.Order;
import com.atguigu.eduorder.mapper.OrderMapper;
import com.atguigu.eduorder.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-03-13
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {


    @Resource
    private EduClient eduClient;

    @Resource
    private UcenterClient ucenterClient;

    /**
     * 根据课程id和 用户id创建订单，返回订单id
     * @param courseId  课程id
     * @param memberId   会员id
     * @return  订单号
     */
    @Override
    public String createOrder(String courseId, String memberId) {

        //1.远程调用根据课程id获取课程信息
        CourseWebVoOrder courseInfoOrder = eduClient.getCourseInfoOrder(courseId);

        //2.远程调用服务根据会员id获取会员信息
        UcenterMemberOrder memberInfo = ucenterClient.getMemberInfo(memberId);

        //3.创建订单，创建order对象，向order对象里面设置所需要的数据
        Order order =new Order();
        order.setOrderNo(OrderNoUtils.getOrderNo());//订单号
        order.setCourseId(courseId);//课程id
        order.setCourseTitle(courseInfoOrder.getTitle());//课程名称
        order.setCourseCover(courseInfoOrder.getCover());//课程封面
        order.setTeacherName(courseInfoOrder.getTeacherName());//讲师名称
        order.setMemberId(memberId);
        order.setNickname(memberInfo.getNickname());//会员昵称
        order.setMobile(memberInfo.getMobile());//会员手机
        order.setTotalFee(courseInfoOrder.getPrice());//订单金额
        order.setPayType(1);//支付类型： 1微信
        order.setStatus(0);//订单状态（0：未支付 1：已支付）
        baseMapper.insert(order);

        //返回订单号
        return order.getOrderNo();
    }
}