package com.atguigu.educms.controller;

import com.atguigu.commonutils.R;
import com.atguigu.educms.entity.CrmBanner;
import com.atguigu.educms.service.CrmBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @descriptions: 首页用户前台
 * @author: pandengZhu
 * @date: 2024/3/31 18:39
 * @version: 1.0
 */

@Api(value = "网页首页banner列表")
@RestController
//@CrossOrigin
@RequestMapping("/educms/bannerfront")
public class BannerFrontController {

    @Resource
    private CrmBannerService bannerService;


    /**
     * 获取首页banner
     * @return  list
     */
    @ApiOperation("获取首页所有banner")
    @GetMapping("/getAllBanner")
    public R getAllBanner() {
        List<CrmBanner> list = bannerService.selectIndexList();
        return R.ok().data("list", list);
    }
}
