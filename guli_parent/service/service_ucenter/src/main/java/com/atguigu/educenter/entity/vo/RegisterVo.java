package com.atguigu.educenter.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * @descriptions: 注册封装类
 * @author: pandengZhu
 * @date: 2024/4/10 15:54
 * @version: 1.0
 */

@Data
@ApiOperation("注册对象")
public class RegisterVo {

    @ApiModelProperty(value = "昵称")
    private String nickName;
    @ApiModelProperty(value = "手机号")
    private String mobile;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "验证码")
    private String code;

}
