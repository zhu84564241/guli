package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
