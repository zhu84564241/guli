package com.atguigu.oss.controller;

import com.atguigu.oss.service.FileService;
import com.atguigu.commonutils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;

/**
 * @descriptions: FileController
 * @author: pandengzhu
 * @date: 2024/3/17 14:47
 * @version: 1.0
 */
@Api(tags = "阿里云文件管理")
//@CrossOrigin
@RestController
@RequestMapping("/eduoss/file")
public class FileController {

    @Resource
    private FileService fileService;

    /**
     * @return
     */
    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public R upload(@ApiParam(value = "文件", required = true)
                    @RequestParam("file") MultipartFile file) {

        try {
            InputStream inputStream = file.getInputStream();
            String originalFilename = file.getOriginalFilename();
            String uploadUrl = fileService.upload(inputStream,originalFilename);
            //返回R对象
            return R.ok().message("文件上传成功").data("url", uploadUrl);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
