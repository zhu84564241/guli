package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
