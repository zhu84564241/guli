package com.atguigu.eduservice.controller.front;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @descriptions: 前端课程名师
 * @author: pandengZhu
 * @date: 2024/4/1 11:57
 * @version: 1.0
 */

//@CrossOrigin
@RestController
@RequestMapping("/eduservice/indexfront")
public class IndexFrontController {

    @Resource
    private EduCourseService courseService;

    @Resource
    private EduTeacherService teacherService;

    /**
     * 查询前八条热门课程，查询前四名讲师
     * @return eduCourseList    eduTeacherList
     */
    @ApiOperation("查询前八条热门课程，查询前四名讲师")
    @GetMapping("/index")
    public R index() {
        //查询前八条热门课程
        QueryWrapper<EduCourse> courseQueryWrapper = new QueryWrapper<>();
        courseQueryWrapper.orderByDesc("id")
                .last("limit 8");
        List<EduCourse> eduCourseList = courseService.list(courseQueryWrapper);

        //查询前四名讲师
        QueryWrapper<EduTeacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id")
                .last("limit 4");
        List<EduTeacher> eduTeacherList = teacherService.list(teacherQueryWrapper);
        return R.ok().data("eduCourseList", eduCourseList).data("eduTeacherList", eduTeacherList);
    }
}
