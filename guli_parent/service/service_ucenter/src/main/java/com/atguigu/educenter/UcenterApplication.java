package com.atguigu.educenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @descriptions:
 * @author: pandengZhu
 * @date: 2024/4/10 17:21
 * @version: 1.0
 */

@EnableFeignClients
@MapperScan("com.atguigu.educenter.mapper")
@ComponentScan(basePackages = {"com.atguigu"})
@SpringBootApplication
public class UcenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(UcenterApplication.class, args);
    }
}
