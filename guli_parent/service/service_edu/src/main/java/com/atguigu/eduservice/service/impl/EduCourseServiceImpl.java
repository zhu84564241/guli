package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.frontvo.CourseFrontVo;
import com.atguigu.eduservice.entity.frontvo.CourseWebVo;
import com.atguigu.commonutils.ResponseEnum;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduCourseDescription;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.atguigu.eduservice.mapper.EduCourseMapper;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduCourseDescriptionService;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.servicebase.exception.BusinessException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Resource
    private EduCourseDescriptionService courseDescriptionService;

    @Resource
    private EduVideoService eduVideoService;

    @Resource
    private EduChapterService eduChapterService;

    /**
     * 添加课程基本信息的方法
     *
     * @param courseInfoVo 自定义course表单对象
     * @return 新生成的课程id
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String addCourseInfo(CourseInfoVo courseInfoVo) {

        //保存课程基本信息
        EduCourse course = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo, course);
        int insert = baseMapper.insert(course);
        if (insert == 0) {
            throw new BusinessException(ResponseEnum.SAVE_COURSE__ERROR.getCode(), ResponseEnum.SAVE_COURSE__ERROR.getMessage());
        }

        String courseId = course.getId();
        //2 向课程简介表添加课程简介
        //edu_course_description
        EduCourseDescription courseDescription = new EduCourseDescription();

        //设置课程描述id就是课程id
        courseDescription.setId(courseId);
        courseDescription.setDescription(courseInfoVo.getDescription());
        courseDescriptionService.save(courseDescription);

        return courseId;
    }

    /**
     * 根据courseId查询课程信息
     *
     * @param courseId 课程id
     * @return
     */
    @Override
    public CourseInfoVo getCourseInfo(String courseId) {

        //1.查询eduCourse课程表
        EduCourse eduCourse = baseMapper.selectById(courseId);
        CourseInfoVo courseInfoVo = new CourseInfoVo();

        //2.查询描述表
        EduCourseDescription eduCourseDescription = courseDescriptionService.getById(courseId);
        courseInfoVo.setDescription(eduCourseDescription.getDescription());

        BeanUtils.copyProperties(eduCourse, courseInfoVo);
        return courseInfoVo;

    }

    /**
     * 修改课程信息
     *
     * @param courseInfoVo 课程封装类
     * @return
     */
    @Override
    public void updateCourseInfo(CourseInfoVo courseInfoVo) {
        //1.修改课程表
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo, eduCourse);
        int update = baseMapper.updateById(eduCourse);
        if (update == 0) {
            throw new BusinessException(ResponseEnum.UPDATE_COURSE__ERROR.getCode(), ResponseEnum.UPDATE_COURSE__ERROR.getMessage());
        }

        //2.修改courseInfoVo的简介
        EduCourseDescription eduCourseDescription = new EduCourseDescription();
        eduCourseDescription.setId(courseInfoVo.getId());
        eduCourseDescription.setDescription(courseInfoVo.getDescription());
        courseDescriptionService.updateById(eduCourseDescription);
    }

    /**
     * 根据课程id查询课程确认信息
     *
     * @param id
     * @return
     */
    @Override
    public CoursePublishVo getPublishCourseInfo(String id) {
        CoursePublishVo publishCourseInfo = baseMapper.getPublishCourseInfo(id);
        return publishCourseInfo;
    }

    /**
     * 根据id发布课程
     *
     * @param id
     * @return
     */
    @Override
    public void publishCourseById(String id) {
        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(id);
        eduCourse.setStatus("Normal");
        baseMapper.updateById(eduCourse);
    }

    /**
     * 删除课程
     *
     * @param courseId
     * @return
     */
    @Override
    public void removeCourse(String courseId) {

        //1.删除小结
        eduVideoService.removeVideoByCourseId(courseId);

        //2.删除章节
        eduChapterService.removeChapterByCourseId(courseId);

        //3.删除描述
        courseDescriptionService.removeById(courseId);

        //4.删除课程
        int result = baseMapper.deleteById(courseId);
        if (result == 0) {
            throw new BusinessException(ResponseEnum.DELETE_COURSE_ERROR.getCode(), ResponseEnum.DELETE_COURSE__ERROR.getMessage());
        }
    }

    /**
     * 分页查询课程列表
     *
     * @param pageParam
     * @param eduCourse
     * @return
     */
    @Override
    public IPage<EduCourse> listPage(Page<EduCourse> pageParam, EduCourse eduCourse) {

        String title = eduCourse.getTitle();

        QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
        if (eduCourse == null) {
            baseMapper.selectPage(pageParam, eduCourseQueryWrapper);
        }
        eduCourseQueryWrapper.like(StringUtils.isNotBlank(title), "title", title)
                .orderByDesc("gmt_create");

        return baseMapper.selectPage(pageParam, eduCourseQueryWrapper);

    }

    /**
     * 前端首页课程分页列表
     *
     * @param pageParam
     * @return
     */
    @Override
    public Map<String, Object> getCourseFrontList(Page<EduCourse> pageParam, CourseFrontVo courseFrontVo) {

        //1.根据讲师id查询所讲课程
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();

        //判断条件值是否为空，不为空拼接
        if (StringUtils.isNotBlank(courseFrontVo.getSubjectParentId())) { //一级分类
            queryWrapper.eq("subject_parent_id", courseFrontVo.getSubjectParentId());
        }

        if (StringUtils.isNotBlank(courseFrontVo.getSubjectId())) {  //二级分类
            queryWrapper.eq("subject_id", courseFrontVo.getSubjectId());
        }

        if (StringUtils.isNotBlank(courseFrontVo.getBuyCountSort())) {   //关注度
            queryWrapper.orderByDesc("buy_count");
        }

        if (StringUtils.isNotBlank(courseFrontVo.getGmtCreateSort())) {  //最新
            queryWrapper.orderByDesc("gmt_create");
        }

        if (StringUtils.isNotBlank(courseFrontVo.getPriceSort())) {  //价格
            queryWrapper.orderByDesc("price");
        }

        baseMapper.selectPage(pageParam, queryWrapper);

        List<EduCourse> records = pageParam.getRecords();
        long current = pageParam.getCurrent();
        long pages = pageParam.getPages();
        long size = pageParam.getSize();
        long total = pageParam.getTotal();
        boolean hasPrevious = pageParam.hasPrevious();//上一页
        boolean hasNext = pageParam.hasNext();//下一页

        //把分页数据获取出来，放到map集合
        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasPrevious", hasPrevious);
        map.put("hasNext", hasNext);


        return map;
    }

    /**
     * 根据课程id，编写sql语句查询课程信息
     *
     * @param courseId
     * @return
     */
    @Override
    public CourseWebVo getCourseFrontInfo(String courseId) {

        return baseMapper.getCourseFrontInfo(courseId);
    }


}
