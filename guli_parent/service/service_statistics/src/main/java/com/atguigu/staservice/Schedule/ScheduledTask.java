package com.atguigu.staservice.Schedule;

import com.atguigu.staservice.DateUtils;
import com.atguigu.staservice.service.StatisticsDailyService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @descriptions: 定时器
 * @author: panDengZhu
 * @date: 2024/5/18 10:37
 * @version: 1.0
 */
@Component
public class ScheduledTask {

    @Resource
    private StatisticsDailyService staService;

    /**
     * 每天凌晨一天执行
     */
    @Scheduled(cron = "0 0 1 * *  ?")
    public void task() {
        //获取上一天的日期
        staService.registerCount(DateUtils.formatDate(DateUtils.addDays(new Date(), -1)));
    }
}
