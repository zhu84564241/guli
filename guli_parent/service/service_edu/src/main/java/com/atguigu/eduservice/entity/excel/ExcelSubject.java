package com.atguigu.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @descriptions:   excel实体类
 * @author: pandengzhu
 * @date: 2024/3/18 14:51
 * @version: 1.0
 */
@Data
public class ExcelSubject {

    @ExcelProperty(index = 0)
    private String oneSubjectName;
    @ExcelProperty(index = 1)
    private String twoSubjectName;
  /*  @ExcelProperty("id")
    private Long id;
    @ExcelProperty("父id")
    private Long parentId;*/



}
