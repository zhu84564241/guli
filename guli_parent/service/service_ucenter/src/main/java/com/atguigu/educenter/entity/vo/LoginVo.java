package com.atguigu.educenter.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * @descriptions: 登录对象封装
 * @author: pandengZhu
 * @date: 2024/4/10 16:03
 * @version: 1.0
 */
@Data
@ApiOperation("登录对象")
public class LoginVo {

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "密码")
    private  String password;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

}
