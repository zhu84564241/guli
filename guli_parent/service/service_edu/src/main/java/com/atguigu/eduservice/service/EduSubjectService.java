package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.subject.OneSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-18
 */
public interface EduSubjectService extends IService<EduSubject> {

    /**
     * 添加课程分类
     * 获取上传过来文件，把文件内容读取出来
     * @param file  excel文件
     */
    void addSubject(MultipartFile file, EduSubjectService eduSubjectService);

    /**
     * 课程分页列表(树形)
     */
    List<OneSubject> getAllSubject();

}
