package com.atguigu.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.atguigu.commonutils.ResponseEnum;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.ExcelSubject;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.servicebase.exception.BusinessException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @descriptions: 课程表格监听
 * @author: pandengzhu
 * @date: 2024/3/18 15:01
 * @version: 1.0
 */
@NoArgsConstructor
@Slf4j
public class ExcelSubjectListener extends AnalysisEventListener<ExcelSubject> {

    /**
     * 每隔5条储存数据库，实际使用中可以储存3000条，然后清理list，方便内存回收
     */
    private static final int BATCH_COUNT = 5;
    List<ExcelSubject> list = new ArrayList<>();

    public EduSubjectService eduSubjectService;

    public ExcelSubjectListener(EduSubjectService eduSubjectService) {
        this.eduSubjectService = eduSubjectService;
    }

    //一行一行去读取excel内容
    @Override
    public void invoke(ExcelSubject data, AnalysisContext context) {
        log.info("解析了一条数据", data);

        /*list.add(data);
        if (list.size() >= BATCH_COUNT) {

        }*/
        if (data == null) {
            throw new BusinessException(ResponseEnum.UPLOAD_ERROR.getCode(), ResponseEnum.UPLOAD_ERROR.getMessage());
        }

        //添加一级分类
        EduSubject existOneSubject = this.existOneSubject(eduSubjectService, data.getOneSubjectName());
        if (existOneSubject == null) {//没有相同的一级分类
            existOneSubject = new EduSubject();
            existOneSubject.setParentId("0");
            existOneSubject.setTitle(data.getOneSubjectName());
            eduSubjectService.save(existOneSubject);
        }

        //获取一级分类的id值
        String pid = existOneSubject.getId();

        //添加二级分类
        EduSubject existTwoObject = this.exitTwoSubject(eduSubjectService, data.getTwoSubjectName(), pid);
        if (existTwoObject == null) {
            existTwoObject = new EduSubject();
            existTwoObject.setTitle(data.getTwoSubjectName());
            existTwoObject.setParentId(pid);
            eduSubjectService.save(existTwoObject);
        }

    }

    /**
     * 判断一级分类 不能重复添加
     *
     * @param eduSubjectService
     * @param name              类别名称
     * @return
     */
    private EduSubject existOneSubject(EduSubjectService eduSubjectService, String name) {
        QueryWrapper<EduSubject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("title", name);
        queryWrapper.eq("parent_id", "0");
        EduSubject oneSubject = eduSubjectService.getOne(queryWrapper);
        return oneSubject;
    }

    /**
     * 判断二级分类 不能重复添加
     *
     * @param eduSubjectService
     * @param name              类别名称
     * @param pid               父id
     * @return
     */
    private EduSubject exitTwoSubject(EduSubjectService eduSubjectService, String name, String pid) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", name);
        wrapper.eq("parent_id", pid);
        EduSubject twoSubject = eduSubjectService.getOne(wrapper);
        return twoSubject;
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
