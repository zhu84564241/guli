package com.atguigu.eduservice.service.impl;

import com.atguigu.commonutils.ResponseEnum;
import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.atguigu.eduservice.entity.chapter.VideoVo;
import com.atguigu.eduservice.mapper.EduChapterMapper;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.servicebase.exception.BusinessException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Resource
    private EduVideoService videoService;


    /**
     * 根据课程id查询课程大纲
     *
     * @param courseId 课程id
     * @return
     */
    //@Transactional(rollbackFor = Exception.class)
    @Override
    public List<ChapterVo> getChapterVideo(String courseId) {

        //1 根据课程id查询课程里面所有的章节
        QueryWrapper<EduChapter> eduChapterQueryWrapper = new QueryWrapper<>();
        eduChapterQueryWrapper.eq("course_id", courseId);
        List<EduChapter> eduChapterList = baseMapper.selectList(eduChapterQueryWrapper);

        //2.根据课程id查询课程里面所有的小结
        QueryWrapper<EduVideo> eduVideoQueryWrapper = new QueryWrapper<>();
        eduVideoQueryWrapper.eq("course_id", courseId);
        List<EduVideo> eduVideoList = videoService.list(eduVideoQueryWrapper);

        //创建list集合，用于最终封装
        List<ChapterVo> finalList = new ArrayList<>();

        //3.遍历查询章节list集合进行封装
        //遍历查询章节list集合
        for (EduChapter eduChapter : eduChapterList) {
            //每个章节
            ChapterVo chapterVo = new ChapterVo();
            //eduChapter对象值复制到ChapterVo里面
            BeanUtils.copyProperties(eduChapter, chapterVo);
            //把chapterVo放到最终list集合
            finalList.add(chapterVo);

            //在章节list集合中遍历查询小结
            //创建list集合封装每个一级分类的二级分类
            List<VideoVo> videoVoList = new ArrayList<>();
            for (EduVideo eduVideo : eduVideoList) {
                //判断小结的chapterId和课程id是否一样
                if (eduVideo.getChapterId().equals(eduChapter.getId())) {

                    //每个小结
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(eduVideo, videoVo);
                    videoVoList.add(videoVo);
                }
            }
            //把所有小结放入课程中
            chapterVo.setChildren(videoVoList);
        }
        return finalList;
    }

    /**
     * 删除章节
     *
     * @param chapterId
     * @return
     */
    @Override
    public boolean deleteChapter(String chapterId) {
        //根据chapterId章节id,查询小结表，如果查询到数据，不删除
        QueryWrapper<EduVideo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("chapter_id", chapterId);
        int count = videoService.count(queryWrapper);
        //判断
        if (count > 0) {    //查询出小节，不进行删除
            throw new BusinessException(ResponseEnum.DELETE_COURSE_ERROR.getCode(), ResponseEnum.DELETE_COURSE_ERROR.getMessage());
        } else {        //不能查询数据，进行删除
            int result = baseMapper.deleteById(chapterId);
            //result>0,返回成功   result<=0,返回失败
            return result > 0;
        }
    }

    /**
     * 删除章节
     *
     * @param courseId
     * @return
     */
    @Override
    public boolean removeChapterByCourseId(String courseId) {

        QueryWrapper<EduChapter> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", courseId);
        Integer count = baseMapper.delete(queryWrapper);

        //count>0并且count不等于null，返回true
        return count > 0 && null != count;
    }
}
