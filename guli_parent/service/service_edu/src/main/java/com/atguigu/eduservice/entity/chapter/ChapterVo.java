package com.atguigu.eduservice.entity.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @descriptions: 章节信息
 * @author: pandengzhu
 * @date: 2024/3/23 0:09
 * @version: 1.0
 */

@Data
public class ChapterVo {

    private String id;
    private String title;

    private List<VideoVo> children =new ArrayList<>();
}
