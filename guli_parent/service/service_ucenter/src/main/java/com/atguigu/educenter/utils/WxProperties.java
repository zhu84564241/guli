package com.atguigu.educenter.utils;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @descriptions: 微信配置类
 * @author: pandengZhu
 * @date: 2024/4/13 11:45
 * @version: 1.0
 */

@Data
@Component
@ConfigurationProperties(prefix = "wx.open")
public class WxProperties implements InitializingBean {


    private String appId;
    private String appSecret;
    private String redirectUrl;

    public static String WX_OPEN_APP_ID;
    public static String WX_OPEN_APP_SECRET;
    public static String WX_OPEN_REDIRECT_URL;



    @Override
    public void afterPropertiesSet() throws Exception {

        WX_OPEN_APP_ID=appId;
        WX_OPEN_APP_SECRET=appSecret;
        WX_OPEN_REDIRECT_URL=redirectUrl;
    }
}
