package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.frontvo.CourseWebVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    /**
     * @param id
     * @return
     */
    CoursePublishVo getPublishCourseInfo(String id);

    /**
     * 根据课程id，编写sql语句查询课程信息
     * @param courseId
     * @return
     */
    CourseWebVo getCourseFrontInfo(String courseId);
}
