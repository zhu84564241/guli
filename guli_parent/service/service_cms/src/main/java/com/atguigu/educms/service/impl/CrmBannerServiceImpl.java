package com.atguigu.educms.service.impl;

import com.atguigu.educms.entity.CrmBanner;
import com.atguigu.educms.mapper.CrmBannerMapper;
import com.atguigu.educms.service.CrmBannerService;
import com.atguigu.commonutils.result.RedisKey;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-31
 */
@Service
@Slf4j
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 获取首页所有banner
     *
     * @return list
     */
    @Override
    public List<CrmBanner> selectIndexList() {
        QueryWrapper<CrmBanner> queryWrapper = new QueryWrapper<>();
        //根据id进行降序排列，显示排列之后前两条记录
        queryWrapper.orderByDesc("id")
                .last("limit 2");
        List<CrmBanner> list = baseMapper.selectList(null);
        try {
            redisTemplate.opsForValue().set(RedisKey.GULI_BANNER_LIST,list,5, TimeUnit.MINUTES);
            log.info("数据存入redis");
        } catch (Exception e) {
            //throw new RuntimeException(e);
            log.error("redis服务器异常："+ExceptionUtils.getMessage(e));
        }
        return list;
    }
}
