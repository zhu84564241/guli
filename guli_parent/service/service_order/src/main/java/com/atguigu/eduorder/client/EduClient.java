package com.atguigu.eduorder.client;


import com.atguigu.commonutils.CourseWebVoOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @descriptions:
 * @author: panDengZhu
 * @date: 2024/5/10 21:51
 * @version: 1.0
 */

@Component
@FeignClient("service-edu")
 public interface EduClient {

 @GetMapping("/eduservice/coursefront/getCourseInfoOrder/{id}")
 public CourseWebVoOrder getCourseInfoOrder(@PathVariable("id") String id);

}