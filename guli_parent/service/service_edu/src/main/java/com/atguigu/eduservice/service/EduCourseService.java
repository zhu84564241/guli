package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.frontvo.CourseWebVo;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.atguigu.eduservice.entity.frontvo.CourseFrontVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
public interface EduCourseService extends IService<EduCourse> {

    /**
     * 添加课程基本信息的方法
     * @param courseInfoVo
     * @return
     */
    String addCourseInfo(CourseInfoVo courseInfoVo);

    /**
     * 根据courseId查询课程信息
     * @param courseId 课程id
     * @return
     */
    CourseInfoVo getCourseInfo(String courseId);

    /**
     * 修改课程信息
     * @param courseInfoVo 课程
     * @return
     */
    void updateCourseInfo(CourseInfoVo courseInfoVo);

    /**
     * 根据课程id查询课程确认信息
     * @param id
     * @return
     */
    CoursePublishVo getPublishCourseInfo(String id);

    /**
     * 根据id发布课程
     * @param id
     * @return
     */
    void publishCourseById(String id);

    /**
     * 删除课程
     * @param courseId
     * @return
     */
    void removeCourse(String courseId);

    /**
     * 分页查询课程列表
     * @param pageParam
     * @param eduCourse
     * @return
     */
    IPage<EduCourse> listPage(Page<EduCourse> pageParam, EduCourse eduCourse);

    /**
     * 首页分页课程类别
     * @param pageParam
     * @return
     */
    Map<String, Object> getCourseFrontList(Page<EduCourse> pageParam, CourseFrontVo courseFrontVo);

    /**
     * 根据课程id，编写sql语句查询课程信息
     * @param courseId
     * @return
     */
    CourseWebVo getCourseFrontInfo(String courseId);
}
