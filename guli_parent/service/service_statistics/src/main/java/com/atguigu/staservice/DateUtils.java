package com.atguigu.staservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @descriptions: 日期操作工具类
 * @author: panDengZhu
 * @date: 2024/5/18 11:05
 * @version: 1.0
 */
public class DateUtils {

    private static final String dateFormat = "yyy-MM-dd";

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(date);
    }

    /**
     *在日期date上增加amount天
     * @param date  处理的日期，非null
     * @param amount    要加的天数，可能为负数
     * @return
     */
    public static Date addDays(Date date, int amount) {
        Calendar cal =Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DATE,cal.get(Calendar.DATE)+amount);
        return cal.getTime();
    }
}
