package com.atguigu.eduservice.controller.front;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacherService;
import com.atguigu.commonutils.R;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @descriptions: 用户端讲师前端控制层
 * @author: pandengZhu
 * @date: 2024/4/16 20:47
 * @version: 1.0
 */

@Api(tags = "用户端讲师")
@RestController
//@CrossOrigin
@RequestMapping("/eduservice/teacherfront")
public class TeacherFrontController {

    @Resource
    private EduTeacherService teacherService;

    @Resource
    private EduCourseService courseService;


    /**
     * 分页查询讲师列表
     *
     * @param page  当前页数
     * @param limit 每页记录数
     * @return
     */
    @ApiOperation("分页查询讲师列表")
    @PostMapping("/getTeacherFrontList/{page}/{limit}")
    public R getTeacherFrontList(@ApiParam(value = "当前页数", required = true) @PathVariable long page,
                                 @ApiParam(value = "每页记录数", required = true) @PathVariable long limit) {
        Page<EduTeacher> pageParam = new Page<>(page, limit);
        Map<String, Object> map = teacherService.getTeacherFrontList(pageParam);
        return R.ok().data(map);
    }

    /**
     * 根据讲师id查询讲师详情
     *
     * @param teacherId 讲师id
     * @return
     */
    @ApiOperation("根据讲师id查询讲师详情")
    @GetMapping("/getTeacherFrontInfo/{teacherId}")
    public R getTeacherFrontInfo(@ApiParam(value = "讲师id", required = true) @PathVariable String teacherId) {
        //1.根据讲师id查询讲师基本信息
        EduTeacher eduTeacher = teacherService.getById(teacherId);
        //2.根据讲师id查询所属课程
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("teacher_id", teacherId);
        List<EduCourse> list = courseService.list(queryWrapper);
        return R.ok().data("teacher", eduTeacher).data("courseList", list);
    }

}
