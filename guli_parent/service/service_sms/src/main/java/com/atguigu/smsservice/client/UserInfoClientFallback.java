package com.atguigu.smsservice.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @descriptions:
 * @author: pandengZhu
 * @date: 2024/4/9 13:57
 * @version: 1.0
 */

@Service
@Slf4j
public class UserInfoClientFallback implements UserInfoClient{
   /* @Override
    public boolean checkMobile(String mobile) {
        log.error("远程调用失败，服务熔断");
        return false;
    }*/
}
