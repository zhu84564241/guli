package com.atguigu.educenter.controller;


import com.atguigu.commonutils.*;
import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.service.UcenterMemberService;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.atguigu.commonutils.result.RedisKey;
import com.atguigu.servicebase.exception.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author pandengzhu
 * @since 2024-04-10
 */
@Api(tags = "会员登录注册")
//@CrossOrigin
@RestController
@RequestMapping("/educenter/member")
public class UcenterMemberController {

    @Resource
    private UcenterMemberService memberService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @ApiOperation("会员注册")
    @PostMapping("/register")
    public R register(@ApiParam(value = "注册", required = true)
                      @RequestBody RegisterVo registerVo) {

        String mobile = registerVo.getMobile();
        String password = registerVo.getPassword();
        String code = registerVo.getCode();

        //1.判断手机号是否为空
        Assert.notEmpty(mobile, ResponseEnum.MOBILE_NULL_ERROR);
        //2.正则表达式判断手机号格式是否正确
        Assert.isTrue(RegexValidateUtils.checkCellphone(mobile), ResponseEnum.MOBILE_ERROR);
        //3.判断密码是否为空
        Assert.notEmpty(password, ResponseEnum.PASSWORD_NULL_ERROR);
        //4.验证码不能为空
        Assert.notEmpty(code, ResponseEnum.CODE_NULL_ERROR);
        //5.从redis获取验证码
        String codeGen = stringRedisTemplate.opsForValue().get(RedisKey.GULI_SMS_CODE + mobile);
        //6.判断验证码是否一致
        Assert.equals(code, codeGen, ResponseEnum.CODE_ERROR);
        //7.注册
        memberService.register(registerVo);
        return R.ok().message("注册成功");
    }


    /**
     * 用户登录
     *
     * @param ucenterMember
     * @return
     */
    @ApiOperation("会员登录")
    @PostMapping("/login")
    public R loginUser(@ApiParam(value = "登录", required = true)
                       @RequestBody UcenterMember ucenterMember) {
        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();

        //1.判断手机号是否为空
        Assert.notEmpty(mobile, ResponseEnum.MOBILE_NULL_ERROR);
        //2.判断密码是否为空
        Assert.notEmpty(password, ResponseEnum.CODE_NULL_ERROR);
        String token = memberService.login(ucenterMember);
        return R.ok().data("token", token);
    }

   /* @ApiOperation("校验令牌")
    @GetMapping("/checkToken")
    public R checkToken(HttpServletRequest request) {

        //String token = request.getHeader("token");
        //boolean result = JwtUtil.checkToken(token);
        boolean result= JwtUtils.getMemberIdByJwtToken(request).isEmpty();

        if (result) {
            return R.ok();
        } else {
            return R.setResult(ResponseEnum.LOGIN_AUTH_ERROR);
        }
    }*/

    @ApiOperation(value = "根据token获取会员登录信息")
    @GetMapping("/auth/getMemberInfo")
    public R getMemberInfo(HttpServletRequest request) {
        //String token = request.getHeader("token");
        //String memberId = JwtUtil.getUserId(token);
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        UcenterMember ucenterMember = memberService.getLoginById(memberId);
        System.out.println("    ucenterMember =      " + ucenterMember.toString());
        return R.ok().data("userInfo", ucenterMember);

    }

    @ApiOperation("获取会员信息")
    @GetMapping("/getMemberInfo/{id}")
    public UcenterMemberOrder getMemberInfo(@ApiParam(value = "用户id", required = true)
                                            @PathVariable String id) {
        //1.根据用户id获取用户信息
        UcenterMember ucenterMember = memberService.getById(id);
        //2.把member对象里面的值复制给UcenterMemberOrder对象
        UcenterMemberOrder memberOrder = new UcenterMemberOrder();
        BeanUtils.copyProperties(ucenterMember, memberOrder);
        return memberOrder;
    }

    @ApiOperation("统计某一天的注册人数")
    @GetMapping("/countRegister/{day}")
    public R countRegister(@ApiParam(value = "日期",required = true)
                           @PathVariable String day) {

        Integer count = memberService.countRegister(day);
        return R.ok().data("countRegister",count);

    }

}

