package com.atguigu.servicebase.exception;

import com.atguigu.commonutils.ResponseEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author pandengzhu
 * @time 2024-03-12 18:20
 */
@Data
@NoArgsConstructor
public class BusinessException extends RuntimeException {

    @ApiModelProperty(value = "状态码")
    private Integer code;
    private String msg;

    /**
     * @param code  错误码
     * @param msg   错误信息
     */
    public BusinessException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     *
     * @param msg  错误信息
     */
    public BusinessException(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @param cause     原始异常对象
     * @param code      错误码
     * @param msg       错误信息
     */
    public BusinessException(Throwable cause, Integer code, String msg) {
        super(cause);
        this.code = code;
        this.msg = msg;
    }

    /**
     * @param resultCodeEnum 接收枚举类型
     */
    public BusinessException(ResponseEnum resultCodeEnum) {
        this.msg = resultCodeEnum.getMessage();
        this.code = resultCodeEnum.getCode();
    }

    /**
     * @param resultCodeEnum 接收枚举类型
     * @param cause          原始异常对象
     */
    public BusinessException(ResponseEnum resultCodeEnum, Throwable cause) {
        super(cause);
        this.msg = resultCodeEnum.getMessage();
        this.code = resultCodeEnum.getCode();
    }

}






