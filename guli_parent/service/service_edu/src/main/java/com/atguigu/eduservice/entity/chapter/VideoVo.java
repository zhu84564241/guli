package com.atguigu.eduservice.entity.chapter;

import lombok.Data;

/**
 * @descriptions: 课时信息
 * @author: pandengzhu
 * @date: 2024/3/23 0:10
 * @version: 1.0
 */

@Data
public class VideoVo {

    private String id;
    private String title;

    private String videoSourceId;   //视频id
}
