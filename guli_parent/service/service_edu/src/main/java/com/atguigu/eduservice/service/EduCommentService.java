package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduComment;
import com.atguigu.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author panDengZhu
 * @since 2024-04-21
 */
public interface EduCommentService extends IService<EduComment> {

    /**
     * 分页查询评论
     * @param pageParam
     * @param eduComment
     * @return
     */
    Map<String, Object> getCommentList(Page<EduComment> pageParam, EduComment eduComment);
}
