package com.atguigu.eduorder.service;

import com.atguigu.eduorder.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-03-13
 */
public interface PayLogService extends IService<PayLog> {

    /**
     * 生成微信支付的二维码
     * @param orderNo   订单号
     * @return
     */
    Map createNative(String orderNo);

    /**
     * 查询订单支付状态
     * @param orderNo   订单号
     * @return
     */
    Map<String, String> queryPayStatus(String orderNo);

    /**
     * 添加支付记录和更新订单状态
     * @param map
     */
    void updateOrdersStatus(Map<String, String> map);
}
