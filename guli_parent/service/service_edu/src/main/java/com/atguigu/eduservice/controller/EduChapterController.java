package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.atguigu.eduservice.service.EduChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
//@CrossOrigin
@RestController
@Api(tags = "课程大纲列表")
@RequestMapping("/eduservice/chapter")
public class EduChapterController {

    @Resource
    private EduChapterService chapterService;

    /**
     * 根据课程id查询课程大纲
     *
     * @param courseId 课程id
     * @return
     */
    @GetMapping("/getChapterVideo/{courseId}")
    @ApiOperation("课程大纲列表")
    public R getChapterVideo(@ApiParam(value = "课程id", required = true)
                             @PathVariable String courseId) {
        List<ChapterVo> list = chapterService.getChapterVideo(courseId);
        return R.ok().data("list", list);
    }

    /**
     * 新增章节
     *
     * @param eduChapter
     * @return
     */
    @ApiOperation("新增章节")
    @PostMapping("/addChapter")
    public R addChapter(@ApiParam(value = "章节", required = true)
                        @RequestBody EduChapter eduChapter) {
        chapterService.save(eduChapter);
        return R.ok().message("添加章节成功");
    }

    /**
     * 根据章节id进行查询
     *
     * @param chapterId
     * @return
     */
    @ApiOperation("根据章节id查询")
    @GetMapping("/getChapterInfo/{chapterId}")
    public R getChapterInfo(@ApiParam(value = "章节id", required = true)
                            @PathVariable String chapterId) {
        EduChapter eduChapter = chapterService.getById(chapterId);
        return R.ok().data("chapter", eduChapter);
    }

    /**
     * 修改章节
     *
     * @param eduChapter
     * @return
     */
    @ApiOperation("修改章节")
    @PostMapping("/updateChapter")
    public R updateChapter(@RequestBody EduChapter eduChapter) {
        chapterService.updateById(eduChapter);
        return R.ok();
    }

    /**
     * 删除章节
     *
     * @param chapterId
     * @return
     */
    @ApiOperation("删除章节")
    @DeleteMapping("/{chapterId}")
    public R deleteChapter(@ApiParam(value = "章节id", required = true)
                           @PathVariable String chapterId) {
        boolean flag = chapterService.deleteChapter(chapterId);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }


}

