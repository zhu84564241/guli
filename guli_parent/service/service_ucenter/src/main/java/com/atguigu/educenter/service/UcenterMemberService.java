package com.atguigu.educenter.service;

import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.entity.vo.LoginVo;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-04-10
 */
public interface UcenterMemberService extends IService<UcenterMember> {



    /**
     * 用户注册
     * @param registerVo
     */
    void register(RegisterVo registerVo);

    /**
     * 用户登录
     * @param ucenterMember
     * @return
     */
    String login(UcenterMember ucenterMember);

    /**
     * 根据token获取登录信息
     * @param userId
     * @return
     */
    UcenterMember getLoginById(String userId);

    /**
     * 根据openid查询会员信息
     * @param openid
     * @return
     */
    UcenterMember getOpenIdMember(String openid);

    /**
     * 统计某一天的注册人数
     * @param day
     * @return
     */
    Integer countRegister(String day);
}
