package com.atguigu.eduservice.client.fallback;

import com.atguigu.eduservice.client.UcenterClient;
import com.atguigu.eduservice.entity.EduComment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @descriptions:
 * @author: pandengZhu
 * @date: 2024/4/22 12:53
 * @version: 1.0
 */

@Component
@Slf4j
public class UcenterClientFallback implements UcenterClient {

    @Override
    public EduComment getMemberInfo(String memberId) {
        log.error("远程调用失败，服务熔断。。。。。。。。。。");
        return null;
    }
}
