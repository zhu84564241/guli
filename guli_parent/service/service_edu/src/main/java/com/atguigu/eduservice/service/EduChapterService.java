package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
public interface EduChapterService extends IService<EduChapter> {

    /**
     * 根据课程id查询课程大纲
     * @param courseId 课程id
     * @return
     */
    List<ChapterVo> getChapterVideo(String courseId);

    /**
     * 删除章节
     * @param chapterId
     * @return
     */
    boolean deleteChapter(String chapterId);

    /**
     * 删除章节
     * @param courseId
     * @return
     */
    boolean removeChapterByCourseId(String courseId);
}
