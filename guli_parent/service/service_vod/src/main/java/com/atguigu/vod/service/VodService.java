package com.atguigu.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @descriptions:
 * @author: pandengZhu
 * @date: 2024/3/27 0:26
 * @version: 1.0
 */

public interface VodService {


    /**
     * 上传视频
     * @param file 视频文件
     * @return  视频id
     */
    String uploadAlyVideo(MultipartFile file);

    /**
     * 删除视频
     * @param id
     */
    void removeVideo(String id);


    /**
     * 批量删除视频
     * @param videoIdList
     */
    void removeMoreAlyVideo(List videoIdList) throws ExecutionException, InterruptedException;

    /**
     * 获取视频播放凭证
     * @param id
     * @return
     */
    String getVideoPlayAuth(String id);
}
