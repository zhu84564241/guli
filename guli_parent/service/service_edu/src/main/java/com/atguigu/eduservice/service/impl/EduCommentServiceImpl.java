package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduComment;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.mapper.EduCommentMapper;
import com.atguigu.eduservice.service.EduCommentService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author panDengZhu
 * @since 2024-04-21
 */
@Service
public class EduCommentServiceImpl extends ServiceImpl<EduCommentMapper, EduComment> implements EduCommentService {


    /**
     * 分页查询评论
     * @param pageParam
     * @param eduComment
     * @return
     */
    @Override
    public Map<String, Object> getCommentList(Page<EduComment> pageParam, EduComment eduComment) {

        QueryWrapper<EduComment> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("course_id",eduComment.getCourseId());

        baseMapper.selectPage(pageParam,queryWrapper);

        List<EduComment> records = pageParam.getRecords();
        long current = pageParam.getCurrent();//当前页
        long size = pageParam.getSize();//总记录数
        long total = pageParam.getTotal();//当前页记录数
        long pages = pageParam.getPages();//总页数
        boolean hasPrevious = pageParam.hasPrevious();//上一页
        boolean hasNext = pageParam.hasNext();//下一页

        Map<String,Object> map = new HashMap<>();
        map.put("items",records);
        map.put("current",current);
        map.put("size",size);
        map.put("total",total);
        map.put("pages",pages);
        map.put("hasPrevious",hasPrevious);
        map.put("hasNext",hasNext);

        return map;
    }
}
