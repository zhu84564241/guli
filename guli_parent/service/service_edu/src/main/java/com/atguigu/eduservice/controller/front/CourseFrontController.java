package com.atguigu.eduservice.controller.front;

import com.atguigu.commonutils.CourseWebVoOrder;
import com.atguigu.commonutils.JwtUtils;
import com.atguigu.eduservice.client.OrderClient;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.atguigu.eduservice.entity.frontvo.CourseFrontVo;
import com.atguigu.eduservice.entity.frontvo.CourseWebVo;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.commonutils.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @descriptions:用户端首页课程
 * @author: pandengZhu
 * @date: 2024/4/17 17:26
 * @version: 1.0
 */

@Api(tags = "用户端首页课程")
//@CrossOrigin
@RestController
@RequestMapping("/eduservice/coursefront")
public class CourseFrontController {

    @Resource
    private EduCourseService courseService;

    @Resource
    private EduChapterService chapterService;

    @Resource
    private OrderClient orderClient;

    /**
     * 首页分页课程列表
     *
     * @param page
     * @param limit
     * @param courseFrontVo
     * @return
     */
    @ApiOperation("分页课程列表")
    @PostMapping("/getCourseFrontList/{page}/{limit}")
    public R getCourseFrontList(@ApiParam(value = "当前页码", required = false) @PathVariable long page,
                                @ApiParam(value = "每页记录数", required = false) @PathVariable long limit,
                                @RequestBody(required = false) CourseFrontVo courseFrontVo) {

        Page<EduCourse> pageParam = new Page<>(page, limit);
        Map<String, Object> map = courseService.getCourseFrontList(pageParam, courseFrontVo);
        return R.ok().data(map);
    }

    @ApiOperation("根据课程id查询课程详情")
    @GetMapping("/getCourseFrontInfo/{courseId}")
    public R getCourseFrontInfo(@ApiParam(value = "课程id", required = true)
                                @PathVariable String courseId, HttpServletRequest request) {
        //根据课程id，编写sql语句查询课程信息
        CourseWebVo courseWebVo = courseService.getCourseFrontInfo(courseId);

        //根据课程id查询章节和小节
        List<ChapterVo> list = chapterService.getChapterVideo(courseId);

        //远程调用，判断课程是否被购买

        //String token = request.getHeader("token");
        //System.out.println("       token:             "+token+"                                         ");
        //String memberId = JwtUtil.getUserId(token);
        String memberId = JwtUtils.getMemberIdByJwtToken(request);

        boolean buyCourse = orderClient.isBuyCourse(courseId, memberId);


        return R.ok().data("courseWebVo", courseWebVo).data("chapterVideoList", list).data("isBuy",buyCourse);
    }

    @ApiOperation("根据课程id获取课程信息")
    @GetMapping("/getCourseInfoOrder/{id}")
    public CourseWebVoOrder getCourseInfoOrder(@ApiParam(value = "课程id", required = true)
                                               @PathVariable String id) {
        CourseWebVo courseInfo = courseService.getCourseFrontInfo(id);
        CourseWebVoOrder courseWebVoOrder = new CourseWebVoOrder();

        BeanUtils.copyProperties(courseInfo, courseWebVoOrder);
        return courseWebVoOrder;

    }

}
