package com.atguigu.educms.mapper;

import com.atguigu.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-31
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
