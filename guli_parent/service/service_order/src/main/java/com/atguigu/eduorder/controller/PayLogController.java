package com.atguigu.eduorder.controller;

import com.atguigu.eduorder.service.PayLogService;
import com.atguigu.commonutils.R;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @descriptions: 支付日志表 前端控制器
 * @author: pandengZhu
 * @date: 2024/5/10 21:51
 * @version: 1.0
 */

@RestController
//@CrossOrigin
@RequestMapping("/eduorder/paylog")
public class PayLogController {

    @Resource
    private PayLogService payLogService;

    /**
     * 生成微信支付的二维码
     *
     * @param orderNo 订单号
     * @return
     */
    @GetMapping("/createNative/{orderNo}")
    public R createNative(@ApiParam(value = "订单号", required = true)
                          @PathVariable String orderNo) {

        //返回信息，包含二维码，还有其他需要的信息
        Map map = payLogService.createNative(orderNo);
        System.out.println("****返回二维码map集合" + map);
        return R.ok().data(map);
    }

    @ApiOperation("添加支付记录&更新支付状态")
    @GetMapping("/queryPayStatus/{orderNo}")
    public R queryPayStatus(@ApiParam(value = "订单号", required = true)
                            @PathVariable String orderNo) {

        //1.调用查询接口
        Map<String, String> map = payLogService.queryPayStatus(orderNo);
        System.out.println("*****查询订单状态map集合:"+map);
        if (map == null) {//
            return R.error().message("支付出错");
        }

        //如果返回map里面不为空，通过map获取订单状态
        if (map.get("trade_state").equals("SUCCESS")){//支付成功
            payLogService.updateOrdersStatus(map);
            return R.ok().message("支付成功");
        }
        return R.ok().code(25000).message("支付中");


    }
}
