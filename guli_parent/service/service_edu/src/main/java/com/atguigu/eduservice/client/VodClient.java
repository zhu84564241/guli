package com.atguigu.eduservice.client;

import com.atguigu.eduservice.client.fallback.VodFileDegradeFeignClient;
import com.atguigu.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @descriptions:
 * @author: pandengZhu
 * @date: 2024/3/29 17:39
 * @version: 1.0
 */
@FeignClient(value = "service-vod",fallback = VodFileDegradeFeignClient.class)
@Component
public interface VodClient {


    /**
     * 定义调用的方法路径
     * 根据视频id删除阿里云视频
     * 服务调用删除视频
     * @PathVariable注解一定要指定参数名称，否则会出错
     * @param id
     * @return
     */
    @DeleteMapping("/eduVod/video/deleteVideo/{videoId}")
    public R removeVideo(@PathVariable("id") String id);


    /**
     *批量删除视频
     * @param videoIdList
     * @return
     */
    @DeleteMapping("/eduVod/video/delete-batch")
    public R deleteBatch(@RequestParam("videoIdList")List<String>videoIdList);


}
