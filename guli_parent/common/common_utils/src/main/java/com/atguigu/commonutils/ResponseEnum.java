 package com.atguigu.commonutils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
/**
 * 返回结果响应码
 */
public enum ResponseEnum {

    SUCCESS(20000, "成功"),
    ERROR(20001, "失败"),

    UPLOAD_ERROR(-103, "文件数据为空"),
    EXPORT_DATA_ERROR(104, "数据导出失败"),

    SAVE_COURSE__ERROR(-105, "课程信息保存失败"),
    UPDATE_COURSE__ERROR(-106, "课程信息修改失败"),
    DELETE_COURSE__ERROR(-1060, "课程信息删除失败"),

    COURSE_STATUS_DRAFT(-107,"课程未发布"),

    DELETE_COURSE_ERROR(-108, "章节信息删除失败"),

    DELETE_VIDEO_ERROR(-109, "删除视频失败"),
    DELETE_BATCH_VIDEO_ERROR(-110, "批量删除视频失败,熔断器--------"),

    SAVE_BANNER_SUCCESS(111, "新增banner成功"),
    LOGIN_AUTH_ERROR(112, "未登录"),
    MOBILE_EXIST_ERROR(-113, "手机号已被注册"),
    MOBILE_NULL_ERROR(-114, "手机号不能为空"),
    MOBILE_ERROR(-115, "手机号错误"),
    ALIYUN_SMS_LIMIT_CONTROL_ERROR(-116, "短信发送过于频繁"),//业务限流
    ALIYUN_SMS_ERROR(-117, "短信发送失败"),
    PASSWORD_NULL_ERROR(-118, "密码不能为空"),
    LOGIN_PASSWORD_ERROR(-119, "密码错误"),
    CODE_ERROR(-120, "验证码错误"),
    CODE_NULL_ERROR(-121, "验证码不能为空"),
    ACCOUNT_DISABLE(-122, "账号被禁用"),

    LOGIN_MOBILE_ERROR(-123,"用户不存在"),

    ADD_UCENTER_MEMBER(124,"添加会员");



    /**
     * 状态响应码
     */
    private final Integer code;

    /**
     * 响应信息
     */
    private final String message;
}
