package com.atguigu.oss.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

/**
 * @descriptions:
 * @author: pandengzhu
 * @date: 2024/3/17 14:23
 * @version: 1.0
 */

public interface FileService {

    /**
     * 文件上传到阿里云Oss
     * @param inputStream
     * @param fileName
     * @return
     */
    String upload(InputStream inputStream,String fileName);
}
