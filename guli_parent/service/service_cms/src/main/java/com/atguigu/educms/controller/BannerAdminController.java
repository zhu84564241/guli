package com.atguigu.educms.controller;


import com.atguigu.commonutils.R;
import com.atguigu.commonutils.ResponseEnum;
import com.atguigu.educms.entity.CrmBanner;
import com.atguigu.educms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 首页banner表 首页管理员后台控制器
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-31
 */
@Api(tags = "首页显示banner数据")
@RestController
//@CrossOrigin
@Slf4j
@RequestMapping("/educms/banneradmin")
public class BannerAdminController {

    @Resource
    private CrmBannerService bannerService;

    /**
     * banner分页查询
     *
     * @param page  页码
     * @param limit 每页记录数
     * @return
     */
    @ApiOperation("banner分页查询")
    @GetMapping("/pageBanner/{page}/{limit}")
    public R pageBanner(@ApiParam(value = "页码", required = true)
                        @PathVariable Long page,
                        @ApiParam(value = "每页记录数", required = true)
                        @PathVariable Long limit) {
        Page<CrmBanner> pageParam = new Page<>(page, limit);
        bannerService.page(pageParam, null);
        return R.ok().data("items", pageParam.getRecords()).data("records", pageParam.getTotal());
    }

    /**
     * 根据id查询banner
     *
     * @param id banner的id
     * @return crmBanner
     */
    @ApiOperation("根据id查询banner")
    @GetMapping("/getBanner/{id}")
    public R getBanner(@ApiParam(value = "banner的id", required = true)
                       @PathVariable String id) {
        CrmBanner crmBanner = bannerService.getById(id);
        return R.ok().data("item", crmBanner);
    }

    /**
     * 新增banner
     *
     * @param crmBanner banner对象
     * @return
     */
    @ApiOperation("新增banner")
    @PostMapping("/saveBanner")
    public R saveBanner(@ApiParam(value = "banner对象", required = true)
                        @RequestBody CrmBanner crmBanner) {
        bannerService.save(crmBanner);
        return R.ok().message(ResponseEnum.SAVE_BANNER_SUCCESS.getMessage());
    }

    /**
     * 修改banner
     *
     * @param crmBanner
     * @return
     */
    @ApiOperation("修改banner")
    @PutMapping("/updateBanner")
    public R updateBanner(@ApiParam(value = "banner对象", required = true)
                          @RequestBody CrmBanner crmBanner) {
        bannerService.updateById(crmBanner);
        return R.ok().message("修改banner成功");
    }

    @ApiOperation("删除banner")
    @DeleteMapping("/remove/{id}")
    public R removeBanner(@ApiParam(value = "banner的id",required = true)
                          @PathVariable String id) {
        bannerService.removeById(id);
        return R.ok().message("删除banner成功");
    }
}

