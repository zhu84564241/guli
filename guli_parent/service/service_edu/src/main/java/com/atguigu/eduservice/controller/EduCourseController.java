package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.atguigu.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author pandengzhu
 * @since 2024-03-19
 */
@RestController
//@CrossOrigin
@Api(tags = "课程管理")
@RequestMapping("/eduservice/course")
public class EduCourseController {

    @Resource
    private EduCourseService eduCourseService;

    /**
     * 添加课程基本信息的方法
     *
     * @param courseInfoVo 编辑课程基本信息的表单对象
     * @return
     */
    @ApiOperation("添加课程基本信息")
    @PostMapping("/addCourseInfo")
    public R addCourseInfo(@ApiParam(value = "新增课程", required = true)
                           @RequestBody CourseInfoVo courseInfoVo) {

        String id = eduCourseService.addCourseInfo(courseInfoVo);

        return R.ok().data("courseId", id);
    }

    /**
     * 根据courseId查询课程信息
     *
     * @param courseId 课程id
     * @return
     */
    @GetMapping("/getCourseInfo/{courseId}")
    @ApiOperation("根据课程id查询课程信息")
    public R getCourseInfoByCourseId(@ApiParam(value = "课程id", required = true)
                                     @PathVariable String courseId) {
        CourseInfoVo courseInfoVo = eduCourseService.getCourseInfo(courseId);
        return R.ok().data("courseInfoVo", courseInfoVo);
    }

    /**
     * 修改课程信息
     *
     * @param courseInfoVo 课程封装类
     * @return
     */
    @ApiOperation("修改课程信息")
    @PostMapping("/updateCourseInfo")
    public R updateCourseInfo(@ApiParam(value = "课程", required = true)
                              @RequestBody CourseInfoVo courseInfoVo) {
        eduCourseService.updateCourseInfo(courseInfoVo);
        return R.ok();
    }


    /**
     * 根据课程id查询课程确认信息
     *
     * @param id
     * @return
     */
    @ApiOperation("根据课程id查询课程确认信息")
    @GetMapping("/getPublishCourseInfo/{id}")
    public R getCoursePublishVoById(@PathVariable String id) {
        CoursePublishVo coursePublishVo = eduCourseService.getPublishCourseInfo(id);
        return R.ok().data("publishCourse", coursePublishVo);
    }

    /**
     * 根据id发布课程
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id发布课程")
    @GetMapping("/publishCourse/{id}")
    public R publishCourseById(@PathVariable String id) {
        eduCourseService.publishCourseById(id);
        return R.ok();
    }

    /**
     * 删除课程
     *
     * @param courseId
     * @return
     */
    @ApiOperation("删除课程")
    @DeleteMapping("/remove/{courseId}")
    public R removeCourse(@PathVariable String courseId) {
        eduCourseService.removeCourse(courseId);
        return R.ok();
    }

    /**
     * 分页查询课程列表
     *
     * @param page      当前页码
     * @param limit     当前页码
     * @param eduCourse
     * @return
     */
    @ApiOperation("分页查询课程列表")
    @PostMapping("/pageCourseCondition/{page}/{limit}")
    public R pageCourseCondition(@ApiParam(value = "当前页码", required = true)
                                 @PathVariable Long page,
                                 @ApiParam(value = "每页记录数", required = true)
                                 @PathVariable Long limit,
                                 @ApiParam(value = "查询条件对象", required = false)
                                 @RequestBody EduCourse eduCourse) {
        Page<EduCourse> pageParam = new Page<>(page, limit);
        IPage<EduCourse> pageModel = eduCourseService.listPage(pageParam, eduCourse);
        return R.ok().data("pageModel", pageModel).message("查询成功");
    }


}

